package userbiz

import (
	"auth_service/common"
	usermodel "auth_service/modules/user/model"
	"context"
	trace "github.com/briansamuel/traceprovider/otel"
)

type ProfileStore interface {
	FindUser(ctx context.Context, conditions map[string]interface{}) (*usermodel.User, error)
}

type profileBiz struct {
	profileStore ProfileStore
}

func NewProfileBiz(profileStore ProfileStore) *profileBiz {
	return &profileBiz{profileStore: profileStore}
}

func (biz *profileBiz) Profile(ctx context.Context, id int) (*usermodel.User, error) {
	user, err := biz.profileStore.FindUser(ctx, map[string]interface{}{"id": id})
	tr := trace.Tracer()
	ctx, span := tr.Start(ctx, "Business Get Profile")
	defer span.End()
	if err != nil {
		return nil, common.ErrEntityNotFound(usermodel.EntityName, err)
	}

	user.Mask(false)
	return user, nil
}
