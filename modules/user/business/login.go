package userbiz

import (
	"auth_service/common"
	"auth_service/component/tokenprovider"
	usermodel "auth_service/modules/user/model"
	"context"
	"fmt"
)

type LoginStore interface {
	FindUser(ctx context.Context, conditions map[string]interface{}) (*usermodel.User, error)
	FindUserDeviceToken(ctx context.Context, conditions map[string]interface{}) (*usermodel.UserDeviceToken, error)
	CreateUserDeviceToken(ctx context.Context, data *usermodel.UserDeviceToken) error
	UpdateUserDeviceToken(ctx context.Context, id int, data *usermodel.UserDeviceToken) error
}

type Cache interface {
	Write(k string, value interface{})
	Read(k string) interface{}
	WriteTTL(k string, value interface{}, exp int)
	Delete(k string)
}

type loginBiz struct {
	cacheStore    Cache
	loginStore    LoginStore
	tokenProvider tokenprovider.Provider
	hasher        Hasher
	expiry        int
}

func NewLoginBiz(cacheStore Cache, loginStore LoginStore, tokenProvider tokenprovider.Provider, hasher Hasher,
	expiry int) *loginBiz {
	return &loginBiz{
		cacheStore:    cacheStore,
		loginStore:    loginStore,
		tokenProvider: tokenProvider,
		hasher:        hasher,
		expiry:        expiry,
	}
}

func (biz *loginBiz) Login(ctx context.Context, data *usermodel.UserLogin) (*tokenprovider.Token, error) {

	if err := data.Validate(); err != nil {
		return nil, err
	}
	user, err := biz.loginStore.FindUser(ctx, map[string]interface{}{"email": data.Email})

	if err != nil {
		return nil, usermodel.ErrEmailOrPasswordInvalid
	}

	passHashed := biz.hasher.Hash(data.Password + user.Salt)

	if user.Password != passHashed {
		return nil, usermodel.ErrEmailOrPasswordInvalid
	}

	payload := tokenprovider.TokenPayload{
		UserId: user.ID,
		Role:   user.Role,
		Device: data.Device,
	}

	accessToken, err := biz.tokenProvider.Generate(payload, biz.expiry)

	if err != nil {
		return nil, common.ErrInternal(err)

	}

	refreshToken := common.RandSequenceFull(32)
	accessToken.RefreshToken = refreshToken

	userDeviceToken, err := biz.loginStore.FindUserDeviceToken(ctx, map[string]interface{}{"user_id": user.ID, "os": data.Device})

	key := fmt.Sprintf("%s", refreshToken)
	if userDeviceToken != nil {

		biz.cacheStore.Delete(userDeviceToken.Token)
		userDeviceToken.Token = refreshToken

		_ = biz.loginStore.UpdateUserDeviceToken(ctx, userDeviceToken.ID, userDeviceToken)

	} else {
		var userDeviceToken = &usermodel.UserDeviceToken{Token: refreshToken, Os: data.Device, IsProduction: true, UserID: user.ID}
		_ = biz.loginStore.CreateUserDeviceToken(ctx, userDeviceToken)

	}
	user.Mask(true)
	biz.cacheStore.WriteTTL(key, user, 30*24*3600)

	return accessToken, nil
}
