package userbiz

import (
	"auth_service/common"
	"auth_service/component/tokenprovider"
	usermodel "auth_service/modules/user/model"
	"context"
	"encoding/json"
	"errors"
	"fmt"
)

type RefreshTokenStore interface {
	FindUser(ctx context.Context, conditions map[string]interface{}) (*usermodel.User, error)
}

type refreshTokenBiz struct {
	cacheStore    Cache
	loginStore    LoginStore
	tokenProvider tokenprovider.Provider
	hasher        Hasher
	expiry        int
}

func NewRefreshTokenBiz(cacheStore Cache, loginStore LoginStore, tokenProvider tokenprovider.Provider, hasher Hasher,
	expiry int) *refreshTokenBiz {
	return &refreshTokenBiz{
		cacheStore:    cacheStore,
		loginStore:    loginStore,
		tokenProvider: tokenProvider,
		hasher:        hasher,
		expiry:        expiry,
	}
}

func (biz *refreshTokenBiz) RefreshToken(ctx context.Context, data *usermodel.UserToken) (*tokenprovider.Token, error) {

	key := fmt.Sprintf("%s", data.RefreshToken)
	userTokenInCache := biz.cacheStore.Read(key)

	if userTokenInCache != nil {
		user := &usermodel.UserCache{}
		err := json.Unmarshal([]byte(userTokenInCache.(string)), user)
		user.Unmask()
		if err != nil {
			return nil, common.ErrInternal(err)
		}
		payload := tokenprovider.TokenPayload{
			UserId: user.ID,
			Role:   user.Role,
			Device: data.OS,
		}

		accessToken, err := biz.tokenProvider.Generate(payload, biz.expiry)

		if err != nil {
			return nil, common.ErrInternal(err)

		}

		return accessToken, nil
	}

	return nil, common.ErrNoPermission(errors.New("refresh token expired"))

}
