package userstore

import (
	"auth_service/common"
	usermodel "auth_service/modules/user/model"
	"context"
	"gorm.io/gorm"
)

func (s *sqlStore) FindUser(ctx context.Context, conditions map[string]interface{}) (*usermodel.User, error) {

	db := s.db.WithContext(ctx).Table(usermodel.User{}.TableName())

	var user usermodel.User

	if err := db.Where(conditions).First(&user).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, common.RecordNotFound
		}

		return nil, common.ErrDB(err)
	}

	return &user, nil
}

func (s *sqlStore) FindUserDeviceToken(ctx context.Context, conditions map[string]interface{}) (*usermodel.UserDeviceToken, error) {

	db := s.db.WithContext(ctx).Table(usermodel.UserDeviceToken{}.TableName())

	var user usermodel.UserDeviceToken

	if err := db.Where(conditions).First(&user).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, common.RecordNotFound
		}

		return nil, common.ErrDB(err)
	}

	return &user, nil
}
