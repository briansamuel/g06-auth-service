package userstore

import (
	"auth_service/common"
	usermodel "auth_service/modules/user/model"
	"context"
)

func (s *sqlStore) Update(ctx context.Context, id int, data *usermodel.UserUpdate) error {
	db := s.db.WithContext(ctx)

	if err := db.Where("id = ?", id).Updates(&data).Error; err != nil {
		return common.ErrDB(err)
	}

	return nil
}

func (s *sqlStore) UpdateUserDeviceToken(ctx context.Context, id int, data *usermodel.UserDeviceToken) error {
	db := s.db.WithContext(ctx)

	if err := db.Where("id = ?", id).Updates(&data).Error; err != nil {
		return common.ErrDB(err)
	}

	return nil
}
