package model

import "net/mail"

func (u *UserCreate) Validate() error {
	_, err := mail.ParseAddress(u.Email)
	if err != nil {
		return ErrEmailValidateInvalid
	}

	if len(u.Password) < 6 {
		return ErrPasswordWeak
	}
	return nil
}

func (data *UserLogin) Validate() error {
	_, err := mail.ParseAddress(data.Email)
	if err != nil {
		return ErrEmailValidateInvalid
	}

	if len(data.Password) < 6 {
		return ErrPasswordWeak
	}

	s := []string{"ios", "android", "web", "window"}

	if !contains(s, data.Device) {
		return ErrDeviceValid
	}

	return nil
}

// contains checks if a string is present in a slice
func contains(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}

	return false
}
