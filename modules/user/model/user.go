package model

import (
	"auth_service/common"
	"time"
)

const EntityName = "User"

type User struct {
	common.SQLModel `json:",inline"`
	Email           string       `json:"email" gorm:"column:email;"`
	Password        string       `json:"-" gorm:"column:password;"`
	Salt            string       `json:"-" gorm:"column:salt"`
	LastName        string       `json:"last_name" gorm:"column:last_name;"`
	FirstName       string       `json:"first_name" gorm:"column:first_name;"`
	Role            string       `json:"role" gorm:"column:role;"`
	Avatar          common.Image `json:"avatar" gorm:"avatar"`
}

func (User) TableName() string { return "users" }

func (u *User) Mask(isOwnerOrAdmin bool) {
	u.GenUID(common.DbTypeUser)
}

func (u *User) GetUserId() int {
	return u.ID
}

func (u *User) GetEmail() string {
	return u.Email
}

func (u *User) GetRole() string {
	return u.Role
}

type UserCreate struct {
	common.SQLModel `json:",inline"`
	Email           string        `json:"email" gorm:"column:email;"`
	Password        string        `json:"password" gorm:"column:password;"`
	Salt            string        `json:"salt" gorm:"column:salt"`
	LastName        string        `json:"last_name" gorm:"column:last_name;"`
	FirstName       string        `json:"first_name" gorm:"column:first_name;"`
	Role            string        `json:"role" gorm:"column:role;"`
	Avatar          *common.Image `json:"avatar" gorm:"avatar"`
}

func (UserCreate) TableName() string { return User{}.TableName() }

func (u *UserCreate) Mask(isOwnerOrAdmin bool) {
	u.GenUID(common.DbTypeUser)
}

type UserLogin struct {
	Email    string `json:"email" gorm:"column:email;"`
	Password string `json:"password" gorm:"column:password;"`
	Device   string `json:"device" gorm:"-"`
}

func (UserLogin) TableName() string { return User{}.TableName() }

type UserUpdate struct {
	common.SQLModel `json:",inline"`
	Password        string        `json:"password" gorm:"column:password;"`
	Salt            string        `json:"salt" gorm:"column:salt"`
	LastName        string        `json:"last_name" gorm:"column:last_name;"`
	FirstName       string        `json:"first_name" gorm:"column:first_name;"`
	Avatar          *common.Image `json:"avatar" gorm:"column:avatar"`
	Lat             float64       `json:"lat" gorm:"column:lat"`
	Lng             float64       `json:"lng" gorm:"column:lng"`
}

func (UserUpdate) TableName() string { return User{}.TableName() }

type UserProfile struct {
	common.SQLModel `json:",inline"`
	LastName        string        `json:"last_name" gorm:"column:last_name;"`
	FirstName       string        `json:"first_name" gorm:"column:first_name;"`
	Avatar          *common.Image `json:"avatar" gorm:"avatar"`
}

func (UserProfile) TableName() string { return User{}.TableName() }

type UserCache struct {
	ID        int           `json:"-" gorm:"id"`
	FakeID    string        `json:"id" gorm:"-"`
	Email     string        `json:"email" gorm:"column:email;"`
	LastName  string        `json:"last_name" gorm:"column:last_name;"`
	FirstName string        `json:"first_name" gorm:"column:first_name;"`
	Role      string        `json:"role" gorm:"column:role;"`
	Avatar    *common.Image `json:"avatar" gorm:"avatar"`
	Status    int           `json:"status" gorm:"column:status;default:1;"`
	Token     string        `json:"-" gorm:"-"`
}

func (UserCache) TableName() string { return User{}.TableName() }

func (u *UserCache) Unmask() {
	uid, _ := common.FromBase58(u.FakeID)
	u.ID = int(uid.GetLocalID())
}

type UserToken struct {
	ID           int    `json:"id" gorm:"-"`
	OS           string `json:"os" gorm:"-"`
	RefreshToken string `json:"refresh_token" gorm:"-"`
	Expired      int    `json:"expired" gorm:"-"`
}

type UserDeviceToken struct {
	ID           int        `json:"id" gorm:"id"`
	UserID       int        `json:"user_id" gorm:"user_id"`
	IsProduction bool       `json:"is_production" gorm:"column:is_production;"`
	Os           string     `json:"os" gorm:"column:os;"`
	Token        string     `json:"token" gorm:"token"`
	Status       int        `json:"status" gorm:"column:status;default:1;"`
	CreatedAt    *time.Time `json:"createdAt" gorm:"column:created_at;"` // Thêm * vào type để lấy null
	UpdatedAt    *time.Time `json:"updatedAt" gorm:"column:updated_at;"` // Thêm * vào type để lấy null
}

func (UserDeviceToken) TableName() string { return "user_device_tokens" }
