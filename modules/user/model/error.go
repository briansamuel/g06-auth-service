package model

import (
	"auth_service/common"
	"errors"
)

var ErrEmailOrPasswordInvalid *common.AppError = common.NewCustomError(
	errors.New("email or password invalid"),
	"email or password invalid",
	"ErrUsernameOrPasswordInvalid",
)

var ErrEmailExisted *common.AppError = common.NewCustomError(
	errors.New("email existed"),
	"email existed",
	"ErrEmailExisted",
)

var ErrEmailNotExisted *common.AppError = common.NewCustomError(
	errors.New("email not existed"),
	"email not existed",
	"ErrEmailNotExisted",
)

var ErrEmailValidateInvalid *common.AppError = common.NewCustomError(
	errors.New("email validate invalid"),
	"email validate invalid",
	"ErrEmailValidateInvalid",
)

var ErrPasswordWeak *common.AppError = common.NewCustomError(
	errors.New("password is too weak"),
	"password is too weak, the minimum is 6 characters",
	"ErrPasswordWeak",
)

var ErrDeviceValid *common.AppError = common.NewCustomError(
	errors.New("device invalid"),
	"please choice device from ios, android or web",
	"ErrDeviceValid",
)
