package ginuser

import (
	"auth_service/common"
	"auth_service/component/appctx"
	"auth_service/component/hasher"
	"auth_service/component/tokenprovider/jwt"
	"auth_service/memcache"
	userbiz "auth_service/modules/user/business"
	usermodel "auth_service/modules/user/model"
	userstore "auth_service/modules/user/store"
	"github.com/gin-gonic/gin"
	"net/http"
)

func RefreshToken(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {
		var refreshToken usermodel.UserToken

		if err := c.ShouldBind(&refreshToken); err != nil {
			panic(common.ErrInvalidRequest(err))
		}
		db := appContext.GetMainDBConnection()
		tokenProvider := jwt.NewTokenJWTProvider(appContext.SecretKey())
		store := userstore.NewSQLStore(db)
		md5 := hasher.NewMd5Hash()
		refreshBiz := userbiz.NewRefreshTokenBiz(memcache.NewRedisCaching(appContext.RedisCache()), store, tokenProvider, md5, 3600*24*30)

		account, err := refreshBiz.RefreshToken(c.Request.Context(), &refreshToken)
		if err != nil {
			c.AbortWithStatusJSON(400, err)
			return
		}

		c.JSON(http.StatusOK, common.SimpleSuccessResponse(account))
	}
}
