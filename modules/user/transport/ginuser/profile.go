package ginuser

import (
	"auth_service/common"
	"auth_service/component/appctx"
	"github.com/gin-gonic/gin"
	"net/http"
)

func Profile(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {

		u := c.MustGet(common.CurrentUser)

		c.JSON(http.StatusOK, common.SimpleSuccessResponse(u))
	}
}
