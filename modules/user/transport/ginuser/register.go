package ginuser

import (
	"auth_service/common"
	"auth_service/component/appctx"
	"auth_service/component/hasher"
	userbiz "auth_service/modules/user/business"
	usermodel "auth_service/modules/user/model"
	userstore "auth_service/modules/user/store"
	"github.com/gin-gonic/gin"
	"net/http"
)

func Register(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {
		db := appContext.GetMainDBConnection()
		var data usermodel.UserCreate

		if err := c.ShouldBind(&data); err != nil {
			panic(common.ErrInternal(err))
		}
		store := userstore.NewSQLStore(db)
		md5 := hasher.NewMd5Hash()
		registerBiz := userbiz.NewRegisterBiz(store, md5)

		if err := registerBiz.Register(c.Request.Context(), &data); err != nil {
			panic(err)
		}
		data.Mask(false)
		c.JSON(http.StatusOK, common.SimpleSuccessResponse(data.FakeId.String()))
	}
}
