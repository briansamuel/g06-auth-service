package ginuser

import (
	"auth_service/common"
	"auth_service/component/appctx"
	"auth_service/component/hasher"
	"auth_service/component/tokenprovider/jwt"
	"auth_service/memcache"
	userbiz "auth_service/modules/user/business"
	usermodel "auth_service/modules/user/model"
	userstore "auth_service/modules/user/store"
	"github.com/gin-gonic/gin"
	"net/http"
)

func Login(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {
		var loginUserData usermodel.UserLogin

		if err := c.ShouldBind(&loginUserData); err != nil {
			panic(common.ErrInvalidRequest(err))
		}
		db := appContext.GetMainDBConnection()
		tokenProvider := jwt.NewTokenJWTProvider(appContext.SecretKey())
		store := userstore.NewSQLStore(db)
		md5 := hasher.NewMd5Hash()
		loginBiz := userbiz.NewLoginBiz(memcache.NewRedisCaching(appContext.RedisCache()), store, tokenProvider, md5, 3600*24*30)

		account, err := loginBiz.Login(c.Request.Context(), &loginUserData)
		if err != nil {
			c.AbortWithStatusJSON(400, err)
			return
		}

		c.JSON(http.StatusOK, common.SimpleSuccessResponse(account))
	}
}
