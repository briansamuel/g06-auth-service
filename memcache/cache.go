package memcache

import (
	"auth_service/common"
	"sync"
	"time"
)

type Cache interface {
	Write(k string, value interface{})
	Read(k string) interface{}
	WriteTTL(k string, value interface{}, exp int)
	Delete(k string)
}

type Caching struct {
	store  map[string]interface{}
	locker *sync.RWMutex
}

func NewCaching() *Caching {
	return &Caching{
		store:  make(map[string]interface{}),
		locker: new(sync.RWMutex),
	}
}

func (c *Caching) Write(k string, value interface{}) {
	c.locker.Lock()
	defer c.locker.Unlock()
	c.store[k] = value
}

func (c *Caching) Read(k string) interface{} {
	c.locker.Lock()
	defer c.locker.Unlock()
	return c.store[k]
}

func (c *Caching) WriteTTL(k string, value interface{}, exp int) {
	c.locker.Lock()
	defer c.locker.Unlock()
	c.store[k] = value

	go func() {
		defer common.Recover()
		<-time.NewTimer(time.Second * time.Duration(exp)).C
		c.Write(k, nil)
	}()
}

func (c *Caching) Delete(k string) {

}
