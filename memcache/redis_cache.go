package memcache

import (
	"context"
	"encoding/json"
	"github.com/go-redis/redis/v8"
	"time"
)

type RedisCaching struct {
	store *redis.Client
}

func NewRedisCaching(store *redis.Client) *RedisCaching {

	return &RedisCaching{
		store: store,
	}
}

func (c *RedisCaching) Write(k string, value interface{}) {

	p, _ := json.Marshal(value)
	err := c.store.Set(context.Background(), k, p, 0).Err()
	if err != nil {
		panic(err)
	}
}

func (c *RedisCaching) Read(k string) interface{} {
	val, err := c.store.Get(context.Background(), k).Result()
	if err != nil {
		return nil
	}
	return val
}

func (c *RedisCaching) ReadStruct(k string, obj interface{}) interface{} {

	val, err := c.store.Get(context.Background(), k).Result()
	if err != nil {
		return nil
	}
	return json.Unmarshal([]byte(val), obj)
}

func (c *RedisCaching) WriteTTL(k string, value interface{}, exp int) {

	p, _ := json.Marshal(value)
	err := c.store.Set(context.Background(), k, p, time.Second*time.Duration(exp)).Err()
	if err != nil {
		panic(err)
	}

}

func (c *RedisCaching) Delete(k string) {

	err := c.store.Del(context.Background(), k).Err()
	if err != nil {
		panic(err)
	}

}
