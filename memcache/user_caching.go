package memcache

import (
	usermodel "auth_service/modules/user/model"
	"context"
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"sync"
)

type RealStore interface {
	FindUser(ctx context.Context, conditions map[string]interface{}) (*usermodel.User, error)
}

type UserCaching struct {
	store     Cache
	realStore RealStore
	once      *sync.Once
}

func NewUserCaching(store Cache, realStore RealStore) *UserCaching {
	return &UserCaching{
		store:     store,
		realStore: realStore,
		once:      new(sync.Once),
	}
}

func (uc *UserCaching) FindUser(ctx context.Context, conditions map[string]interface{}) (*usermodel.UserCache, error) {
	userId := conditions["id"].(int)
	//device := conditions["device"].(string)
	key := fmt.Sprintf("user-%d", userId)

	userInCache := uc.store.Read(key)
	userCache := &usermodel.UserCache{}
	if userInCache != nil {
		err := json.Unmarshal([]byte(userInCache.(string)), userCache)
		if err != nil {
			panic(err)
		}
		return userCache, nil
	} else {
		user, err := uc.realStore.FindUser(ctx, conditions)
		log.WithField("FindUser", "userInCache is nil").Info(user)
		log.WithField("FindUser", "error").Info(err)
		user.Mask(true)
		if err != nil {
			panic(err)
		}

		if user != nil {
			uc.store.WriteTTL(key, user, 3600)
		}

	}

	if err := json.Unmarshal([]byte(uc.store.Read(key).(string)), userCache); err != nil {
		return nil, err
	}
	return userCache, nil
}
