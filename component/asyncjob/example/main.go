package main

import (
	"auth_service/component/asyncjob"
	"context"
	"log"
	"time"
)

func main() {
	job1 := asyncjob.NewJob(func(ctx context.Context) error {
		time.Sleep(time.Second)
		log.Println("I am job 1")

		return nil
		//return errors.New("some thing went wrong at job 1")
	})

	job2 := asyncjob.NewJob(func(ctx context.Context) error {
		time.Sleep(time.Second)
		log.Println("I am job 2")

		return nil
	})

	job3 := asyncjob.NewJob(func(ctx context.Context) error {
		time.Sleep(time.Second * 2)
		log.Println("I am job 3")

		return nil
	}, asyncjob.WithName("Somthing with Job 3"))
	//if err := job1.Execute(context.Background()); err != nil {
	//	log.Println("error:", err)
	//
	//	for {
	//		err := job1.Retry(context.Background())
	//
	//		if err == nil || job1.State() == asyncjob.StateRetryFailed {
	//			log.Println("Try Failed:")
	//			break
	//		}
	//	}
	//}

	group := asyncjob.NewGroup(true, job1, job2, job3)

	if err := group.Run(context.Background()); err != nil {
		log.Println(err)
	}
}
