package appctx

import (
	"github.com/briansamuel/traceprovider"
	"github.com/go-redis/redis/v8"
	"gorm.io/gorm"
)

type AppContext interface {
	GetMainDBConnection() *gorm.DB
	SecretKey() string
	TraceProvider() traceprovider.Provider
	RedisCache() *redis.Client
}

type appCtx struct {
	db            *gorm.DB
	secretKey     string
	traceProvider traceprovider.Provider
	redisClient   *redis.Client
}

func NewAppContext(db *gorm.DB, secretKey string, traceProvider traceprovider.Provider, redisClient *redis.Client) *appCtx {
	return &appCtx{db: db, secretKey: secretKey, traceProvider: traceProvider, redisClient: redisClient}
}

func (ctx *appCtx) GetMainDBConnection() *gorm.DB { return ctx.db }

func (ctx *appCtx) SecretKey() string { return ctx.secretKey }

func (ctx *appCtx) TraceProvider() traceprovider.Provider { return ctx.traceProvider }

func (ctx *appCtx) RedisCache() *redis.Client { return ctx.redisClient }
