package tokenprovider

import (
	"auth_service/common"
	"errors"
	"time"
)

type Provider interface {
	Generate(data TokenPayload, expiry int) (*Token, error)
	Validate(token string) (*TokenPayload, error)
}

var (
	ErrNotFound *common.AppError = common.NewCustomError(
		errors.New("token not found"),
		"token not found",
		"ErrNotFound")

	ErrEncodingToken *common.AppError = common.NewCustomError(
		errors.New("error encoding token"),
		"error encoding token",
		"ErrEncodingToken")

	ErrInvalidToken *common.AppError = common.NewCustomError(
		errors.New("error invalid token"),
		"error invalid token",
		"ErrInvalidToken")
)

type Token struct {
	Token        string    `json:"token"`
	RefreshToken string    `json:"refreshToken"`
	Created      time.Time `json:"created"`
	Expiry       int       `json:"expiry"`
}

type TokenPayload struct {
	UserId int    `json:"user_id"`
	Role   string `json:"role"`
	Device string `json:"device"`
}
