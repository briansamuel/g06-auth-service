package middleware

import (
	"auth_service/common"
	"auth_service/component/appctx"
	"auth_service/component/tokenprovider/jwt"
	usermodel "auth_service/modules/user/model"
	"context"
	"errors"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"strings"
)

type AuthenStore interface {
	FindUser(ctx context.Context, conditions map[string]interface{}) (*usermodel.UserCache, error)
}

func ErrWrongAuthHeader(err error) *common.AppError {
	return common.NewCustomError(
		errors.New("error wrong authen header"),
		"error wrong authen header",
		"ErrWrongAuthHeader",
	)
}

func extractTokenFromHeaderString(s string) (string, error) {
	parts := strings.Split(s, " ")

	if parts[0] != "Bearer" || len(parts) < 2 || strings.TrimSpace(parts[1]) == "" {
		return "", ErrWrongAuthHeader(nil)
	}
	return parts[1], nil
}

// 1. Get token from header
// 2. Validate Token and parse to payload
// 3. From the token Payload, we use user_id to find from DB
func RequireAuth(appCtx appctx.AppContext, authstore AuthenStore) func(c *gin.Context) {
	tokenProvider := jwt.NewTokenJWTProvider(appCtx.SecretKey())

	return func(c *gin.Context) {
		tr := appCtx.TraceProvider().GetTracer()
		spanContext := appCtx.TraceProvider().ParentContext(c.Request.Context(), c.GetHeader("TraceID"), c.GetHeader("SpanID"))
		_, span := tr.Start(spanContext, "Authentication Middleware")
		span.AddEvent("processing....") // WORKING
		token, err := extractTokenFromHeaderString(c.GetHeader("Authorization"))
		if err != nil {

			span.SetStatus(codes.Error, err.Error())
			panic(err)
		}

		payload, err := tokenProvider.Validate(token)
		if err != nil {

			log.WithField("middleware", "tokenProvider Validate").Info(err)
			span.End()
			panic(err)
		}

		user, err := authstore.FindUser(c.Request.Context(), map[string]interface{}{"id": payload.UserId})
		if err != nil {

			log.WithField("middleware", "authStore FindUser").Info(err)
			span.End()
			panic(err)
		}
		if user.Status == 0 {
			log.WithField("ErrNoPermission", user).Info()
			panic(common.ErrNoPermission(errors.New("user has been deleted or banner")))
		}

		span.SetAttributes(attribute.Key("user_id").Int(user.ID))
		span.End()

		c.Set(common.CurrentUser, user)
		c.Next()
	}
}
