package common

import (
	"errors"
	"fmt"
	"net/http"
)

type AppError struct {
	StatusCode int    `json:"status_code"`
	RootErr    error  `json:"-"`
	Message    string `json:"message"`
	Log        string `json:"log"`
	Key        string `json:"error_key"`
}

func NewErrorResponse(root error, msg, log, key string) *AppError {
	return &AppError{
		StatusCode: http.StatusBadRequest,
		RootErr:    root,
		Message:    msg,
		Log:        log,
		Key:        key,
	}
}

func NewFullErrorResponse(statusCode int, root error, msg, log, key string) *AppError {
	return &AppError{
		StatusCode: statusCode,
		RootErr:    root,
		Message:    msg,
		Log:        log,
		Key:        key,
	}
}

func NewUnauthorized(root error, msg, key string) *AppError {
	return &AppError{
		StatusCode: http.StatusUnauthorized,
		RootErr:    root,
		Message:    msg,
		Key:        key,
	}
}

func NewCustomError(root error, msg, key string) *AppError {
	if root != nil {
		return NewErrorResponse(root, msg, root.Error(), key)
	}
	return NewErrorResponse(errors.New(msg), msg, msg, key)
}
func (e *AppError) RootError() error {
	if err, ok := e.RootErr.(*AppError); ok {
		return err.RootError()
	}
	return e.RootErr
}

func (e *AppError) Error() string {
	return e.RootError().Error()
}

func ErrDB(err error) *AppError {
	return NewErrorResponse(err, "some thing went wrong with DB", err.Error(), "ErrDB")
}

func ErrInvalidRequest(err error) *AppError {
	return NewErrorResponse(err, "invalid request", err.Error(), "ErrInvalidRequest")
}

func ErrInternal(err error) *AppError {
	return NewFullErrorResponse(http.StatusInternalServerError, err, "some thing went wrong in the server", err.Error(), "ErrInternal")
}

func ErrCannotListEntity(entity string, err error) *AppError {
	return NewCustomError(err, fmt.Sprintf("cannot list %s", entity), fmt.Sprintf("ErrCannotList%s", entity))
}

func ErrCannotGetEntity(entity string, err error) *AppError {
	return NewCustomError(err, fmt.Sprintf("cannot get %s", entity), fmt.Sprintf("ErrCannotGet%s", entity))
}

func ErrCannotDeleteEntity(entity string, err error) *AppError {
	return NewCustomError(err, fmt.Sprintf("cannot delete %s", entity), fmt.Sprintf("ErrCannotDelete%s", entity))
}

func ErrEntityNotFound(entity string, err error) *AppError {
	return NewCustomError(err, fmt.Sprintf("entity %s not found", entity), fmt.Sprintf("Err%sNotFound", entity))
}

func ErrEntityExist(entity string, err error) *AppError {
	return NewCustomError(err, fmt.Sprintf("entity %s exist", entity), fmt.Sprintf("Err%sExist", entity))
}

func ErrEntityDeleted(entity string, err error) *AppError {
	return NewCustomError(err, fmt.Sprintf("entity %s deleted", entity), fmt.Sprintf("Err%sDeleted", entity))
}

func ErrCannotCreateEntity(entity string, err error) *AppError {
	return NewCustomError(err, fmt.Sprintf("cannot create %s", entity), fmt.Sprintf("ErrCannotGreate%s", entity))
}

func ErrCannotUpdateEntity(entity string, err error) *AppError {
	return NewCustomError(err, fmt.Sprintf("cannot update %s", entity), fmt.Sprintf("ErrCannotUpdate%s", entity))
}
func ErrNoPermission(err error) *AppError {
	return NewUnauthorized(err, fmt.Sprintf("error no permission"), fmt.Sprintf("ErrNoPermission"))
}

func ErrFieldCannotBlank(entity string, err error) *AppError {
	return NewCustomError(err, fmt.Sprintf("field required cannot blank of entity %s", entity), fmt.Sprintf("ErrField%sCannotBlank", entity))
}

var RecordNotFound = errors.New("record not found")
